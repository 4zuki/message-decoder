# Secret Message Decoder
Here is a fun encryptor & decryptor. It takes a message and scrambles the alphabet based off of the keyword used. This is not my original idea, but I came up with the code on my own. The original challenge is just with plain alphabet characters but I included numbers and spacing as a possibility as well.

To see this in action, go to [My Website](http://bone.voyage/md/message_decoder.html)

### Shuffling the alphabet based on a keyword
We begin with the standard alphabet:
>ABCDEFGHIJKLMNOPQRSTUVWXYZ

..And a keyword. For example, use **KEYWORD** as the keyword and shuffle the alphabet before and after each letter in the keyword. So to start, swap around the letter **K**:
>ABCDEFGHIJKLMNOPQRSTUVWXYZ\
LMNOPQRSTUVWXYZ **K** ABCDEFGHIJ

Then swap around the letter **E**:

>LMNOPQRSTUVWXYZKABCDEFGHIJ\
FGHIJ **E** LMNOPQRSTUVWXYZKABCD

And so on, until the final letter of the keyword is used.
>ZKABCD **Y** FGHIJELMNOPQRSTUVWX\
X **W** ZKABCDYFGHIJELMNOPQRSTUV\
PQRSTUV **O** XWZKABCDYFGHIJELMN\
STUVOXWZKABCDYFGHIJELMN **R** PQ\
YDGHIJELMNRPQ **D** STUVOXWZKABC

The final alphabet of this process becomes the initial alphabet of the encryption or decryption

### Description of the cipher
The encryption process uses the same shuffling type. For each character, a substitution is made, and then a shuffling. Each shuffle swaps the block of letters before the character with the block of letters after it. For example, use **KEYWORD** as a keyword again and using the message "send help". First, find the substitution for **S**:
>abcdefghijklmnopqr **s** tuvwxyz\
YDGHIJELMNRPQDSTUV **O** XWZKABC

We get an **O**. Next, shuffle the alphabet around the letter **s**:
>YDGHIJELMNRPQDSTUVOXWZKABC\
TUVOXWZKABC **S** YDGHIJELMNRPQD

This new alphabet will be used to encrypt the next letter of the message:
>abcd **e** fghijklmnopqrstuvwxyz\
TUVO **X** WZKABCSYDGHIJELMNRPQD

We get **X**. Next, shuffle around the letter **e**:
>TUVOXWZKABCSYDGHIJELMNRPQD\
LMNRPQD **E** TUVOXWZKABCSYDGHIJ

This continues until the entire message is encrypted.
>sendhelp\
OXWDZMSA
