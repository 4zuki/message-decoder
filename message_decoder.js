const alphabetOrig = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ 0123456789';

function runEncoder() {
    const keyword = textMessage('keywordValue');
    const message = textMessage('secretMessageValue');

    //scramble alphabetOrig based on keyword
    scrambleAlphabet(keyword, true, alphabetOrig, true, function(newAlphabet) {
        //continue to scramble for each letter in the message
        scrambleAlphabet(message, false, newAlphabet, true);
    });
}

function runDecoder() {
    const keyword = textMessage('encryptedKeywordValue');
    const message = textMessage('encryptedMessageValue');

    //scramble alphabetOrig based on keyword
    scrambleAlphabet(keyword, true, alphabetOrig, true, function(newAlphabet) {
        //scramble for each letter while also recording original message
        scrambleAlphabet(message, false, newAlphabet, false);
    });
}

function textMessage (htmlID) {
    const t = document.getElementById(htmlID).value;
    let cleanText = t.replace(/[^a-z0-9]/gi, ' ');
    cleanText = cleanText.toUpperCase();
    return cleanText;
}

//scramble alphabet based on the keyword/message (textObject)
function scrambleAlphabet (textObject, isKeyword, alphabet, encrypt, callback) {
    //alphabet to be scrambled
    let alphabetEncrypted = alphabet;
    //encrypted message to be returned
    let eMessage = '';

    //swap the alphabet around either side of each letter in the textObject
    for (let letter in textObject) {
        //current letter position in the alphabet
        let indexPos;
        let currentLetter;

        if (encrypt) {
            indexPos = alphabetOrig.search(textObject.charAt(letter));
            currentLetter = textObject.charAt(letter);
            eMessage += alphabetEncrypted.charAt(indexPos);
        } else {
            indexPos = alphabetEncrypted.search(textObject.charAt(letter));
            currentLetter = alphabetOrig.charAt(indexPos);
            eMessage += currentLetter;
        }

        //split the alphabet around textObject's letter
        let aSplit = alphabetEncrypted.split(currentLetter);
        //swap the alphabet on either side of textObject's letter
        aSplit.reverse();
        //insert the textObject letter back into the middle of the alphabet array
        aSplit.splice(1, 0, currentLetter);
        //turn the array back to a string
        alphabetEncrypted = aSplit.join('');
    }

    //print encrypted message (&& not the encrypted keyword) to html display
    if (encrypt && !isKeyword) {
        const copyBtn = document.getElementById('copyBtn');
        copyBtn.style.display = 'block';
        document.getElementById('encryptedMessage').innerHTML = eMessage;
    } else if (!encrypt && !isKeyword) {
        document.getElementById('decryptedMessage').innerHTML = eMessage;
    }

    //then pass new alphabet and callback next step
    if (typeof callback === 'function') {
        callback(alphabetEncrypted);
    }
}

function copyMsg() {
    const eMessage = document.getElementById('encryptedMessage');
    eMessage.focus();
    eMessage.setSelectionRange(0, eMessage.value.length);
    document.execCommand("copy");
}
